# @emotive/emotive-loader

## 1.2.1

### Patch Changes

- 0b12cc2: Remove console log and update comment

## 1.2.0

### Minor Changes

- 0acd823: Adds a new interface for specifying which federated module to load.

## 1.1.1

### Patch Changes

- 766429c: Upgrades third-party dependencies

## 1.1.0

### Minor Changes

- e54e209: Upgrades dependencies

## 1.0.0

### Major Changes

- a86c228: Adds loadFederatedModule method

### Minor Changes

- baf6c42: Updates dependencies
- 6ea4bcb: Updates third-party dependencies
