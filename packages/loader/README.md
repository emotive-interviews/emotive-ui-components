# @emotive/emotive-loader

Asynchronous loader fetching front end assets.

## Quick Start

Add a link to the stylesheet into your HTML template/page:

```html
<script
  data-origin="https://emotivecdn.io"
  data-overrides='{"navigation": "https://dev-cdn.emotiveapp.dev"}'
  src="//emotivecdn.io/emotive-loader/lib.js"
></script>
```

Once loaded, all of the loader's methods will be available via the `window.emotiveLoader` object. You can load federated modules as follows:

```html
<script>
  window.emotiveLoader
    .loadFederatedModule({
      moduleName: 'navigation',
      modulePath: './NavShell',
    })
    .then(function ({ default: bootstrap }) {
      // use the module.
    });
</script>
```

## API

### `loadFederatedModuleContainer(moduleName: string): Promise<Container>`

_Retrieves a Federated Module container from the base origin or overridden origin, using the specified `moduleName`._

**NOTE** This method is primarily used for integrating federated modules between Webpack apps.

Parameters:

- `moduleName:string`: the name of the module to load.

### `loadFederatedModule({ moduleName, modulePath }): Promise<Module>`

_Retrieves a Federated Module from the base origin overridden origin, using the `module` and `modulePath` parameters to configure the load parameters._

Parameters:

- `moduleName:string`: the name of the module to load.
- `modulePath:string`: the path to the desired object/function in the federated module.

### `overrides`

Provides an interface for overriding the `origin` for one or more federated modules. This is useful for validating a merge request, local development, etc.

**NOTE**: Overridden values are stored in the browser where these methods are executed. They will not affect other people's applications in any way.

### `overrides.clear(): void`

_Clears any stored override values._

Example:

```js
window.emotiveLoader.overrides.clear();
```

### `overrides.entries(): Map<string,string>`

_Returns a map containing any overridden values._

Example:

```js
window.emotiveLoader.overrides.entries();
```

### `overrides.get(moduleName: string): string|undefined`

_Returns the overridden value for the specified module, or `undefined` if none is found._

Example:

```js
window.emotiveLoader.get('navigation');
```

### `overrides.set(moduleName: string, overriddenOrigin: string): void`

_Overrides the origin for the specified module with the specified origin value._

Example:

```js
window.emotiveLoader.set('navigation', 'http://localhost:8080/'); // Microfrontend is running locally.
window.emotiveLoader.set(
  'analytics-ui',
  'https://review-cdn.emotiveapp.dev/my-branch/',
); // Perhaps the origin for a merge request.
window.emotiveLoader.set('broadcast-ui', 'https://stg-cdn.emotiveapp.dev/'); // Switch to a different environment's microfrontend.
```

## Development

The following scripts are available:

- `build`: generates a production build.
- `build --analyze`: generates a production build, and opens the analysis view using `webpack-bundle-analyzer`.
- `lint`: lints the project.
- `test`: executes the tests.
- `test:ci`: runs the tests, and generates a code coverage report.

NOTE: the script(s) can be run using either:

- `pnpm --filter @emotive/emotive-loader {script}`: when running from the root of the project.
- `pnpm {script}`: when running from this directory.
