import {
  createMockFederatedContainer,
  mockWebpackGlobals,
} from './federated-modules.test-helpers';
import { initializeFederatedModuleFrom } from './federated-modules';

beforeEach(() => {
  jest.clearAllMocks();
});

describe('initializeFederatedModuleFrom', () => {
  test('initializes the federated module container', async () => {
    mockWebpackGlobals();
    const { container, mockModule } = createMockFederatedContainer();

    const modulePath = './bootstrap';
    const actual = await initializeFederatedModuleFrom(container, modulePath);

    expect(__webpack_init_sharing__).toHaveBeenCalledWith('default');
    expect(container.init).toHaveBeenCalledWith(
      __webpack_share_scopes__.default,
    );
    expect(container.get).toHaveBeenCalledWith(modulePath);
    expect(actual).toBe(mockModule);
  });
});
