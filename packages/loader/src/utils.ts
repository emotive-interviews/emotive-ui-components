import { pascalCase } from 'change-case';

// PIN the cachebuster parameter so it's consistent for a given page load.
const NOW = Date.now();

interface ModuleUrlParams {
  origin: string;
  moduleName: string;
}

function createUrl({ moduleName, origin }: ModuleUrlParams): string {
  const url = new URL(`${moduleName}/remote-entry.js`, origin);
  // Append cachebusting value.
  url.search = `?cb=${NOW}`;

  return url.toJSON();
}

function moduleToScopeName(moduleName: string): string {
  return pascalCase(`E${moduleName}`);
}

export { createUrl, moduleToScopeName };
