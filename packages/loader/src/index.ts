import loadjs from 'loadjs';
import { initializeFederatedModuleFrom } from './federated-modules';
import { initialize } from './initializer';
import { createOverrideableUrl } from './overrideable-urls';
import { moduleToScopeName } from './utils';

// TODO: remove the parameters below and just always use `baseOrigin` as `origin` once `data-origin` is in all sensus environments.
const { origin: baseOrigin, overrides, store } = initialize();

type ModuleParameters = {
  origin: string | undefined;
  moduleName: string;
  modulePath: string;
};

// API for loading and initializing Federated modules outside of Webpack.
// See https://webpack.js.org/concepts/module-federation/ for source of much of the code.

/**
 * Loads the Federated module container for the specified module.
 * NOTE: this methods is primarily used for integrating federated modules with Webpack.
 *
 * @param moduleName - the name of the module to load
 * @param origin - the origin to use when the store has no override value for the module
 * @returns the Federated module container.
 */
async function loadFederatedModuleContainer(
  moduleName: string,
  origin = baseOrigin,
): Promise<Container> {
  const loadableUrl = createOverrideableUrl({ moduleName, origin }, store);
  await loadjs(loadableUrl, { returnPromise: true });
  return window[moduleToScopeName(moduleName)];
}

/**
 * Loads the federated module from the specified federated module container.
 *
 * @param param.moduleName - the name of the container module to load
 * @param param.modulePath - the name of the module file within the federated module container to load and return
 * @param param.origin - the origin to use when the store has no override value for the module
 * @returns the initialized module
 */
async function loadFederatedModule({
  moduleName,
  modulePath,
  origin = baseOrigin, // NOTE: origin is only needed until changes are live in all envs.
}: ModuleParameters): Promise<Module> {
  const container = await loadFederatedModuleContainer(moduleName, origin);
  return await initializeFederatedModuleFrom(container, modulePath);
}

export { loadFederatedModuleContainer, loadFederatedModule, overrides };
