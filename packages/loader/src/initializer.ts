import {
  createJSONStorageHandler,
  createLocalStorageHandler,
} from './stores/storage-handlers';
import {
  createChainableReadableStore,
  createMutableStore,
  createReadableStore,
} from './stores/stores';

const EMOTIVE_LOADER_STORE_KEY = '$$EMOTIVE_LOADER_OVERRIDES$$';

function initialize() {
  const script = document.currentScript as HTMLScriptElement;

  // NOTE: this is only needed until the changes in sensus-dev are live in all envs.
  const url = new URL(script.src);
  const origin = script.dataset.origin ?? url.origin;
  const serverOverrides = script.dataset.overrides ?? '{}';

  const overrides = createMutableStore(
    createLocalStorageHandler(EMOTIVE_LOADER_STORE_KEY),
  );
  const store = createChainableReadableStore(
    createReadableStore(createJSONStorageHandler(serverOverrides)),
    overrides,
  );

  return { origin, overrides, store };
}

export { initialize, EMOTIVE_LOADER_STORE_KEY };
