import type { ReadableHandler, MutableHandler } from './types';
import {
  fromEntriesJSONParser,
  fromObjectJSONParser,
  isInIFrame,
  loadFromJSON,
} from './utils';

function createJSONStorageHandler(sourceJSON?: string): ReadableHandler {
  return {
    load() {
      return loadFromJSON(sourceJSON, fromObjectJSONParser);
    },
  };
}

function createLocalStorageHandler(storageKey: string): MutableHandler {
  if (isInIFrame()) {
    return {
      load() {
        return new Map();
      },
      persist() {
        // noop
      },
    };
  }
  return {
    load() {
      const overrideString = localStorage.getItem(storageKey);
      return loadFromJSON(overrideString, fromEntriesJSONParser);
    },
    persist(store) {
      localStorage.setItem(
        storageKey,
        JSON.stringify(Array.from(store.entries())),
      );
    },
  };
}

export { createJSONStorageHandler, createLocalStorageHandler };
