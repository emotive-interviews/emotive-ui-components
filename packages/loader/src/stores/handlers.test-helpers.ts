import type { MutableHandler, Override, ReadableHandler } from './types';

function createMutableHandler(
  initialData: Record<string, Override>,
): MutableHandler {
  let internalStore = initialData;
  return {
    load() {
      return new Map(Object.entries(internalStore)); // should only be called once, anyways.
    },
    persist(store) {
      internalStore = Object.fromEntries(store);
    },
  };
}

function createReadableHandler(
  initialData: Record<string, Override>,
): ReadableHandler {
  return {
    load() {
      return new Map(Object.entries(initialData));
    },
  };
}

export { createMutableHandler, createReadableHandler };
