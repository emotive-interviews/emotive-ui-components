import type { Override } from './types';

function isInIFrame(): boolean {
  return window.top !== window.self;
}

function fromEntriesJSONParser(sourceJSON: string): Map<string, Override> {
  const overrides = JSON.parse(sourceJSON) as [string, Override][];
  return new Map(overrides);
}

function fromObjectJSONParser(sourceJSON: string): Map<string, Override> {
  const overrides = JSON.parse(sourceJSON) as Record<string, Override>;
  return new Map(Object.entries(overrides));
}

function loadFromJSON(
  sourceJSON: string | null | undefined,
  parser: (sourceJSON: string) => Map<string, Override>,
): Map<string, Override> {
  if (!sourceJSON) {
    return new Map();
  } else {
    try {
      return parser(sourceJSON);
    } catch (e) {
      console.log('Unable to parse the loader overrides', e);
      return new Map();
    }
  }
}

export {
  fromEntriesJSONParser,
  fromObjectJSONParser,
  isInIFrame,
  loadFromJSON,
};
