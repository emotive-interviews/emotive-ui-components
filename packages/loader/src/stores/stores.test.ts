import {
  createReadableHandler,
  createMutableHandler,
} from './handlers.test-helpers';
import {
  createReadableStore,
  createMutableStore,
  createChainableReadableStore,
} from './stores';

describe('readableStore', () => {
  test('initializes with an empty store', () => {
    const handler = createReadableHandler({});
    jest.spyOn(handler, 'load');
    const store = createReadableStore(handler);

    expect(store.get('foo')).toBeUndefined();
    expect(handler.load).toHaveBeenCalled();
  });

  test('retrieves values from loaded store', () => {
    const initialData = {
      foo: 'https://emotivecdn.io/',
      bar: 'https://test-cdn.emotiveapp.dev/',
    };
    const store = createReadableStore(createReadableHandler(initialData));

    expect(store.get('foo')).toEqual('https://emotivecdn.io/');
    expect(store.get('bar')).toEqual('https://test-cdn.emotiveapp.dev/');
    expect(store.get('baz')).toBeUndefined();
  });
});

describe('mutableStore', () => {
  test('initializes with an empty store', () => {
    const handler = createMutableHandler({});
    jest.spyOn(handler, 'load');
    const store = createMutableStore(handler);

    expect(store.entries()).toEqual({});
    expect(store.get('foo')).toBeUndefined();
    expect(handler.load).toHaveBeenCalled();
  });

  test('retrieves values from loaded store', () => {
    const initialData = {
      foo: 'https://emotivecdn.io/',
      bar: 'https://test-cdn.emotiveapp.dev/',
    };
    const store = createMutableStore(createMutableHandler(initialData));

    expect(store.entries()).toEqual(initialData);

    expect(store.get('foo')).toEqual('https://emotivecdn.io/');
    expect(store.get('bar')).toEqual('https://test-cdn.emotiveapp.dev/');
    expect(store.get('baz')).toBeUndefined();
  });

  test('clear empties the store', () => {
    const initialData = {
      foo: 'https://emotivecdn.io/',
      bar: 'https://test-cdn.emotiveapp.dev/',
    };
    const handler = createMutableHandler(initialData);
    jest.spyOn(handler, 'persist');

    const store = createMutableStore(handler);

    expect(store.entries()).toEqual(initialData);

    store.clear();
    expect(store.entries()).toEqual({});
    expect(handler.persist).toHaveBeenCalled();
  });

  test('set updates a single store value', () => {
    const handler = createMutableHandler({});
    jest.spyOn(handler, 'persist');

    const store = createMutableStore(handler);
    store.set('foo', 'https://my-foo.emotiveapp.dev/');

    expect(store.entries()).toEqual({
      foo: 'https://my-foo.emotiveapp.dev/',
    });
    expect(handler.persist).toHaveBeenCalled();
  });
});

describe('chainable readable store', () => {
  test('returns undefined when no stores provided', () => {
    const store = createChainableReadableStore();

    expect(store.get('foo')).toBeUndefined();
    expect(store.get('bar')).toBeUndefined();
  });

  test('wraps a single readable store', () => {
    const store = createChainableReadableStore(
      createReadableStore(
        createReadableHandler({
          foo: 'https://dev-cdn.emotiveapp.dev/',
          bar: 'https://test-cdn.emotiveapp.dev/',
        }),
      ),
    );

    expect(store.get('foo')).toEqual('https://dev-cdn.emotiveapp.dev/');
    expect(store.get('bar')).toEqual('https://test-cdn.emotiveapp.dev/');
  });

  test('resolves stores in order', () => {
    const store = createChainableReadableStore(
      createReadableStore(
        createReadableHandler({
          foo: 'https://foo-cdn.emotiveapp.dev/',
        }),
      ),
      createReadableStore(
        createReadableHandler({
          foo: 'https://dev-cdn.emotiveapp.dev/',
        }),
      ),
    );

    expect(store.get('foo')).toEqual('https://foo-cdn.emotiveapp.dev/');
  });

  test('returns undefined if no store matches', () => {
    const store = createChainableReadableStore(
      createReadableStore(
        createReadableHandler({
          foo: 'https://foo-cdn.emotiveapp.dev/',
        }),
      ),
      createReadableStore(
        createReadableHandler({
          foo: 'https://dev-cdn.emotiveapp.dev/',
        }),
      ),
    );

    expect(store.get('bar')).toBeUndefined();
  });
});
