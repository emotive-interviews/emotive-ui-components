import * as utils from './utils';
import {
  createJSONStorageHandler,
  createLocalStorageHandler,
} from './storage-handlers';

beforeEach(() => {
  // eslint-disable-next-line @typescript-eslint/no-empty-function
  jest.spyOn(console, 'log').mockImplementation(() => {});
});

afterEach(() => {
  jest.resetAllMocks();
});

describe('json-storage-handler', () => {
  test('load returns empty when no string defined', () => {
    const { load } = createJSONStorageHandler();
    const store = load();

    expect(Object.fromEntries(store)).toEqual({});
  });

  test('load returns the correct store for the given input JSON string', () => {
    const input = {
      demo: 'https://dev-cdn.emotiveapp.dev/',
      party: 'https://partycdn.io/',
    };

    const { load } = createJSONStorageHandler(JSON.stringify(input));
    const store = load();

    expect(Object.fromEntries(store)).toEqual(input);
  });
});

describe('local-storage-handler', () => {
  afterEach(() => {
    localStorage.clear();
  });

  test('load returns an empty map when no localstorage value', () => {
    const { load } = createLocalStorageHandler('my-store');
    const store = load();

    expect(Object.fromEntries(store)).toEqual({});
  });

  test('load returns store with localStorage data', () => {
    localStorage.setItem(
      'my-store',
      JSON.stringify([
        ['demo', 'https://dev-cdn.emotiveapp.dev/'],
        ['party', 'https://partycdn.io/'],
      ]),
    );

    const { load } = createLocalStorageHandler('my-store');
    const store = load();

    expect(Object.fromEntries(store)).toEqual({
      demo: 'https://dev-cdn.emotiveapp.dev/',
      party: 'https://partycdn.io/',
    });
  });

  test('load returns an empty map when localstorage entry is unparseable', () => {
    localStorage.setItem('my-store', '}'); // Invalid JSON
    const { load } = createLocalStorageHandler('my-store');
    const store = load();

    expect(Object.fromEntries(store)).toEqual({});
    expect(console.log).toHaveBeenCalledWith(
      'Unable to parse the loader overrides',
      expect.any(Error),
    );
  });

  test('persist stores an empty item', () => {
    const { persist } = createLocalStorageHandler('my-store');

    expect(localStorage.getItem('my-store')).toBeNull();

    persist(new Map());

    expect(localStorage.getItem('my-store')).toEqual('[]');
  });

  test('persist updates loaded data', () => {
    localStorage.setItem(
      'my-store',
      JSON.stringify([
        ['demo', 'https://dev-cdn.emotiveapp.dev/'],
        ['party', 'https://partycdn.io/'],
      ]),
    );

    const { load, persist } = createLocalStorageHandler('my-store');
    const store = load();

    store.set('demo', 'https://my-demo.dev/');

    persist(store);

    const store2 = load();

    expect(store2).toEqual(store);
  });

  test('ignores localStorage when inside of an IFrame', () => {
    jest.spyOn(utils, 'isInIFrame').mockReturnValue(true);

    localStorage.setItem(
      'my-store',
      JSON.stringify([['demo', 'https://dev-cdn.emotiveapp.dev/']]),
    );

    const { load } = createLocalStorageHandler('my-store');
    const store = load();

    expect(Object.fromEntries(store)).toEqual({});
  });

  test('ignores persisting to localStorage when inside of an IFrame', () => {
    jest.spyOn(utils, 'isInIFrame').mockReturnValue(true);

    const { load, persist } = createLocalStorageHandler('my-store');
    const store = load();

    persist(store);

    expect(localStorage.getItem('my-store')).toBeNull();
  });
});
