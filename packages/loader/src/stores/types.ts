export type Override = string;

export interface ReadableHandler {
  load(): Map<string, Override>;
}

export interface MutableHandler extends ReadableHandler {
  persist(store: Map<string, Override>): void;
}

export interface ReadableStore {
  get(moduleName: string): Override | undefined;
}

export interface MutableStore extends ReadableStore {
  clear(): void;
  entries(): Record<string, Override>;
  set(moduleName: string, origin: string): void;
}
