import type {
  MutableHandler,
  MutableStore,
  ReadableHandler,
  ReadableStore,
} from './types';

function createChainableReadableStore(
  ...stores: ReadableStore[]
): ReadableStore {
  return {
    get(moduleName: string) {
      for (const store of stores) {
        const value = store.get(moduleName);
        if (value) {
          return value;
        }
      }
      return undefined;
    },
  };
}

function createReadableStore(handler: ReadableHandler): ReadableStore {
  const store = handler.load();

  return {
    get(moduleName: string) {
      return store.get(moduleName);
    },
  };
}

function createMutableStore(handler: MutableHandler): MutableStore {
  const store = handler.load();

  return {
    clear() {
      store.clear();
      handler.persist(store);
    },
    entries() {
      return Object.fromEntries(store);
    },
    get(moduleName: string) {
      return store.get(moduleName);
    },
    set(moduleName, origin) {
      store.set(moduleName, origin);
      handler.persist(store);
    },
  };
}

export {
  createChainableReadableStore,
  createReadableStore,
  createMutableStore,
};
