function mockWebpackGlobals() {
  __webpack_init_sharing__ = jest.fn().mockResolvedValue(void 0);
  __webpack_share_scopes__ = { default: {} };
}

function createMockFederatedContainer() {
  const mockModule = jest.fn();
  const factoryFn: Factory = jest.fn().mockReturnValue(mockModule);
  const container: Container = {
    init: jest.fn(),
    get: jest.fn().mockImplementation().mockReturnValue(factoryFn),
  };
  return { container, mockModule };
}

export { createMockFederatedContainer, mockWebpackGlobals };
