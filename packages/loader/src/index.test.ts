/* eslint-disable @typescript-eslint/no-var-requires */
import loadjsOriginal from 'loadjs';
import * as federatedModules from './federated-modules';
import {
  createMockFederatedContainer,
  mockWebpackGlobals,
} from './federated-modules.test-helpers';
import { initialize } from './initializer';
import { createMutableStore, createReadableStore } from './stores/stores';
import {
  createMutableHandler,
  createReadableHandler,
} from './stores/handlers.test-helpers';

jest.mock('./initializer');

// eslint-disable-next-line @typescript-eslint/no-explicit-any
function mockFunction<T extends (...args: any[]) => any>(
  fn: T,
): jest.MockedFunction<T> {
  return fn as jest.MockedFunction<T>;
}

const loadjs = mockFunction(loadjsOriginal);

jest.mock('loadjs');

beforeEach(() => {
  jest.clearAllMocks();
  mockFunction(initialize).mockReturnValue({
    origin: 'https://emotivecdn.io/',
    store: createReadableStore(
      createReadableHandler({ foo: 'https://foo.cdn.io/' }),
    ),
    overrides: createMutableStore(
      createMutableHandler({ foo: 'https://foo.cdn.io/' }),
    ),
  });
});

afterEach(() => {
  Object.keys(window)
    .filter((key) => key.startsWith('E'))
    .forEach((key) => {
      delete window[key];
    });
});

test('loads container', async () => {
  const { container } = createMockFederatedContainer();
  window['EmyContainer'] = container;

  loadjs.mockResolvedValue();

  jest.isolateModules(async () => {
    const { loadFederatedModuleContainer } = require('.');
    const fedContainer = await loadFederatedModuleContainer('my-container');

    expect(loadjs).toHaveBeenCalledWith(
      expect.stringMatching(
        'https://emotivecdn.io/my-container/remote-entry.js\\?cb=\\d+',
      ),
      expect.anything(),
    );
    expect(fedContainer).toBe(container);
  });
});

test('loads module', async () => {
  mockWebpackGlobals();
  const { container, mockModule } = createMockFederatedContainer();
  window['EmyModule'] = container;

  loadjs.mockResolvedValue();

  jest.spyOn(federatedModules, 'initializeFederatedModuleFrom');

  jest.isolateModules(async () => {
    const { loadFederatedModule } = require('.');
    const fedModule = await loadFederatedModule({
      moduleName: 'my-module',
      modulePath: './bootstrap',
    });

    expect(loadjs).toHaveBeenCalledWith(
      expect.stringMatching(
        'https://emotivecdn.io/my-module/remote-entry.js\\?cb=\\d+',
      ),
      expect.anything(),
    );

    expect(fedModule).toBe(mockModule);
  });
});

test('exports overrides API', () => {
  jest.isolateModules(() => {
    const { overrides } = require('.');
    expect(overrides).toHaveProperty('clear');
    expect(overrides).toHaveProperty('entries');
    expect(overrides).toHaveProperty('get');
    expect(overrides).toHaveProperty('set');
  });
});
