import type { ReadableStore } from './stores/types';
import { createUrl } from './utils';

const createOverrideableUrl = (
  { moduleName, origin }: Parameters<typeof createUrl>[0],
  store: ReadableStore,
) => {
  const overriddenOrigin = store.get(moduleName);
  if (overriddenOrigin) {
    console.log(`[emotive-loader]: ${moduleName} => ${overriddenOrigin}`);
  }
  return createUrl({ moduleName, origin: overriddenOrigin ?? origin });
};

export { createOverrideableUrl };
