import { createUrl, moduleToScopeName } from './utils';

const DEV_CDN = 'https://dev-cdn.emotiveapp.dev/';
const PROD_CDN = 'https://emotivecdn.io/';
const REVIEW_CDN = 'https://review-cdn.emotiveapp.dev/';
const LOCALHOST = 'http://localhost:8080';

// eslint-disable-next-line @typescript-eslint/no-explicit-any
type TestCaseData<T extends (...args: any) => any> = {
  input: Parameters<T>[0];
  expected: ReturnType<T>;
};

jest.useFakeTimers();

describe('createUrl', () => {
  test.each`
    input                                                              | expected
    ${{ origin: DEV_CDN, moduleName: 'navigation' }}                   | ${`${DEV_CDN}navigation/remote-entry.js`}
    ${{ origin: PROD_CDN, moduleName: 'dashboard-ui' }}                | ${`${PROD_CDN}dashboard-ui/remote-entry.js`}
    ${{ origin: `${REVIEW_CDN}my-branch/`, moduleName: 'navigation' }} | ${`${REVIEW_CDN}my-branch/navigation/remote-entry.js`}
    ${{ origin: LOCALHOST, moduleName: 'party' }}                      | ${`${LOCALHOST}/party/remote-entry.js`}
  `(
    'params generates $expected',
    ({ input, expected }: TestCaseData<typeof createUrl>) => {
      expect(createUrl(input)).toMatch(new RegExp(`${expected}\\?cb=\\d+`));
    },
  );
});

describe('moduleToScopeName', () => {
  test.each`
    input             | expected
    ${'navigation'}   | ${'Enavigation'}
    ${'dashboard-ui'} | ${'EdashboardUi'}
  `(
    'generates $expected for $input',
    ({ input, expected }: TestCaseData<typeof moduleToScopeName>) => {
      expect(moduleToScopeName(input)).toEqual(expected);
    },
  );
});
