const SHARED_SCOPE_NAME = 'default';

async function initializeFederatedModuleFrom(
  container: Container,
  modulePath: string,
): Promise<Module> {
  await __webpack_init_sharing__(SHARED_SCOPE_NAME);
  await container.init(__webpack_share_scopes__[SHARED_SCOPE_NAME]);
  const factory = await container.get(modulePath);
  return factory();
}

export { initializeFederatedModuleFrom };
