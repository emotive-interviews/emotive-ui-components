import { createReadableHandler } from './stores/handlers.test-helpers';
import { createReadableStore } from './stores/stores';
import { createOverrideableUrl } from './overrideable-urls';

const DEV_CDN = 'https://dev-cdn.emotiveapp.dev/';
const PROD_CDN = 'https://emotivecdn.io/';
const REVIEW_CDN = 'https://review-cdn.emotiveapp.dev/';
const LOCALHOST = 'http://localhost:8080/';

// eslint-disable-next-line @typescript-eslint/no-explicit-any
type TestCaseData<T extends (...args: any) => any> = {
  input: Parameters<T>[0];
  expected: ReturnType<T>;
};

jest.useFakeTimers();

describe('createOverrideableUrl', () => {
  const STORE = createReadableStore(
    createReadableHandler({ overridden: LOCALHOST }),
  );

  beforeEach(() => {
    // eslint-disable-next-line @typescript-eslint/no-empty-function
    jest.spyOn(console, 'log').mockImplementation(() => {});
  });

  test.each`
    input                                                              | expected
    ${{ origin: DEV_CDN, moduleName: 'navigation' }}                   | ${`${DEV_CDN}navigation/remote-entry.js`}
    ${{ origin: PROD_CDN, moduleName: 'dashboard-ui' }}                | ${`${PROD_CDN}dashboard-ui/remote-entry.js`}
    ${{ origin: `${REVIEW_CDN}my-branch/`, moduleName: 'navigation' }} | ${`${REVIEW_CDN}my-branch/navigation/remote-entry.js`}
    ${{ origin: DEV_CDN, moduleName: 'overridden' }}                   | ${`${LOCALHOST}overridden/remote-entry.js`}
  `(
    'params generates $expected',
    ({ input, expected }: TestCaseData<typeof createOverrideableUrl>) => {
      expect(createOverrideableUrl(input, STORE)).toMatch(
        new RegExp(`${expected}\\?cb=\\d+`),
      );
    },
  );

  test('logs overriden url', () => {
    createOverrideableUrl({ moduleName: 'overridden', origin: DEV_CDN }, STORE);

    expect(console.log).toHaveBeenCalledWith(
      `[emotive-loader]: overridden => ${LOCALHOST}`,
    );
  });
});
