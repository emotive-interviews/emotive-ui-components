import { EMOTIVE_LOADER_STORE_KEY, initialize } from './initializer';

const SCRIPT = document.createElement('script');
SCRIPT.src = 'https://localhost:8080/lib.js';
Object.defineProperty(document, 'currentScript', {
  value: SCRIPT,
});

afterEach(() => {
  localStorage.clear();
  delete SCRIPT.dataset.origin;
  delete SCRIPT.dataset.overrides;
});

test('initializes overrides store', () => {
  localStorage.setItem(
    EMOTIVE_LOADER_STORE_KEY,
    JSON.stringify([
      ['demo', 'https://dev-cdn.emotiveapp.dev/'],
      ['party', 'https://partycdn.io/'],
    ]),
  );

  const { overrides } = initialize();

  expect(overrides.get('demo')).toEqual('https://dev-cdn.emotiveapp.dev/');
  expect(overrides.get('party')).toEqual('https://partycdn.io/');
});

test('uses origin of currentScript when data-origin is not set', () => {
  const { origin } = initialize();

  expect(origin).toEqual('https://localhost:8080');
});

test('uses data-origin for origin when specified', () => {
  SCRIPT.dataset.origin = 'https://partycdn.io/';

  const { origin } = initialize();

  expect(origin).toEqual('https://partycdn.io/');
});

test('applies data-overrides when defined', () => {
  SCRIPT.dataset.overrides = JSON.stringify({
    foo: 'https://dev-cdn.emotiveapp.dev/',
  });

  const { store } = initialize();

  expect(store.get('foo')).toEqual('https://dev-cdn.emotiveapp.dev/');
});
