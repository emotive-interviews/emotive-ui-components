const {
  createWebpackConfig,
  partials: { babel, federationPlugin },
} = require('@emotive/webpack-build-config');

function asLib(config) {
  return {
    optimization: {
      splitChunks: {
        chunks: 'async',
      },
    },
    output: {
      filename: 'lib.js',
      library: {
        name: config.project.camelCase,
        type: 'umd',
        umdNamedDefine: true,
      },
    },
  };
}

module.exports = function (webpackEnv, argv) {
  return createWebpackConfig(webpackEnv, argv, {
    basePath: __dirname,
    partials: [babel, federationPlugin, asLib],
  });
};
