interface Module {
  [fn: string]: (element: Element, props: Record<string, unknown>) => void;
}

type Scope = Record<string, Container>;
type Factory = () => Module;

type Container = {
  init(scope: Scope): Promise<void> | void;
  get(module: string): Promise<Factory>;
};

declare let __webpack_init_sharing__: (scope: string) => void | Promise<void>;
declare let __webpack_share_scopes__: { default: Scope };

interface Window {
  [scope: string]: Container;
}
