# emotive-ui-components

A mono-repo containing code for the ui components and supporting modules.

## Setup

Checkout the repository

```bash
git clone git@gitlab.com:emotive.io/emotive-ui-components.git
```

Install dependencies

```bash
cd emotive-ui-components
pnpm install
```

## Development

- `pnpm format`: runs [`prettier`][prettier] on all of the code.
- `pnpm lint`: runs [`eslint`][eslint] on the all of the code.

**NOTE** linting and formatting are automatically run on code on pre-commit (using [`husky`][husky] and [`lint-staged`][lint-staged]).

[eslint]: https://eslint.org/
[husky]: https://github.com/typicode/husky#readme
[lint-staged]: https://github.com/okonet/lint-staged#readme
[prettier]: https://prettier.io/
[volta]: https://volta.sh/
